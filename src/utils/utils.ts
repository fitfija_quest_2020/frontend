export function getURL(): string {
  // eslint-disable-next-line no-undef
  if (process.env.NODE_ENV === "production") {
    return "35.228.211.178:8081";
  } else {
    return "localhost:8081";
  }
}
