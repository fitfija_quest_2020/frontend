export interface AlertMessage {
    success: boolean;
    message: string;
}

export interface ApiResponse<T> {
    code: number;
    response: T;
}

export interface ErrorBody {
    error: string;
}

export interface LoginResponseBody {
    token: string;
}

export interface TaskAnswerConfirmationBody {
    result: string;
}

export interface LoginCredentials {
    email: string;
    password: string;
}

export interface ChangePasswordCredentials {
    oldPassword: string;
    newPassword: string;
}


export interface RegistrationCredentials {
    email: string;
    password: string;
    firstName: string;
    lastName: string;
    contactInfo: string;
    isInAcadem: string;
}

export interface User {
    id: number;
    email: string;
    invitationId: number;
    points: number;
    firstName: string;
    lastName: string
    faculty: string;
    contactInfo: string;
    isInAcadem: boolean;
}

export interface LeaderboardEntry {
    name: string;
    points: number;
}

export interface TaskDto {
    id: number;
    created: string;
    updated: string;
    stageId: number;
    isHidden: boolean;
    isCompleted: boolean;
    name: string;
    text: string;
    points: number;
    imageUrl: string;
}

export interface StageDto {
    id: number;
    created: string;
    updated: string;
    name: string;
    start: string;
    end: string;
    tasks: TaskDto[];
}

export interface UserUpdate {
    firstName: string;
    lastName: string;
    contactInfo: string;
    isInAcadem: string;
}

export interface Invitation {
    id: number;
    created: string;
    updated: string;
    senderName: string;
    receiverName: string;
    status: string;
}

export interface InvitationInfoDto {
    hasTeam: boolean;
    teammateName: string;
}

export interface NotificationDto {
    id: number;
    created: string;
    updated: string;
    text: string;
}

export interface NewsDto {
    id: number;
    created: string;
    updated: string;
    text: string;
    headline: string;
}

export interface EmployeeDto {
    id: number;
    created: string;
    updated: string;
    name: string;
    position: string;
    description: string;
    imageUrl: string;
}

export interface IsEnabled {
    result: boolean;
}

export interface TeamDto {
    id: number;
    created: string;
    updated: string;
    name: string;
    players: User[];
    currentPoints: number;
    totalPoints: number;
    type: string;
}

export interface ItemDto {
    id: number;
    created: string;
    updated: string;
    name: string;
    description: string;
    inStock: number;
    price: number;
    imageUrl: string;
}

export interface PurchaseDto {
    id: number;
    created: string;
    updated: string;
    item: ItemDto;
    status: "IN_TRANSIT" | "DELIVERED" | "CANCELED";
}

export type Confirmation = string | true;
export type LoginResponse = LoginResponseBody | ErrorBody;
export type ConfirmationResponse = string | ErrorBody;
export type TaskAnswerConfirmation = TaskAnswerConfirmationBody | ErrorBody;
export type LeaderboardResponse = LeaderboardEntry[] | ErrorBody;
export type UserResponse = User | ErrorBody;
export type TeamResponse = TeamDto | ErrorBody;
export type PurchaseResponse = PurchaseDto[] | ErrorBody;
export type ItemResponse = ItemDto[] | ErrorBody;
export type EnabledResponse = IsEnabled | ErrorBody;
export type InvitationsResponse = Invitation[] | ErrorBody;
export type InvitationInfoResponse = InvitationInfoDto | ErrorBody;
export type NewsResponse = NewsDto[] | ErrorBody;
export type EmployeeResponse = EmployeeDto[] | ErrorBody;
export type StageResponse = StageDto | ErrorBody;
export type TaskResponse = TaskDto | ErrorBody;
export type InvitationInfo = InvitationInfoDto | string;
export type Task = TaskDto | string;
export type Stage = StageDto | string;
export type Team = TeamDto | string;
export type Item = ItemDto[] | string;
export type Purchase = PurchaseDto[] | string;
export type Leaderboard = LeaderboardEntry[] | string;
export type Invitations = Invitation[] | string;
export type News = NewsDto[] | string;
export type Employees = EmployeeDto[] | string;
export type NotificationsResponse = NotificationDto[] | ErrorBody;
export type Notifications = NotificationDto[] | string;
