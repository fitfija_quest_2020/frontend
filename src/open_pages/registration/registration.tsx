import React, {useState, useEffect, FormEvent} from "react";
import {Button, Container, Form, Row} from "react-bootstrap";
import styles from "../../css/login.module.css";
import {Link, useHistory} from "react-router-dom";
import {validateEmail} from "../../utils/validations";
import {RegistrationCredentials} from "../../utils/interfaces";
import {registration} from "../../apis/auth_api";
import Alert from "react-bootstrap/Alert";

export default function Registration(): JSX.Element {
    const [credentials, setCredentials] = React.useState<RegistrationCredentials>({
        email: "",
        password: "",
        firstName: "",
        lastName: "",
        contactInfo: "",
        isInAcadem: "true"
    });
    const [canSubmit, setCanSubmit] = useState<boolean>(false);
    const [show, setShow] = useState<boolean>(false);
    const [alertMsg, setAlertMsg] = useState<string>("");
    const [passwordConfirmation, setPasswordConfirmation] = useState<string>("");

    const history = useHistory();

    const enableAlert = (alert: string): void => {
        if (alert !== undefined) {
            setAlertMsg(alert);
            setShow(true);
        }
    };

    useEffect(() => {
        setCanSubmit(
            validateEmail(credentials.email) &&
            credentials.password.length >= 8 &&
            credentials.password === passwordConfirmation &&
            credentials.contactInfo.length > 0
        );
    }, [credentials.email, credentials.password, credentials.contactInfo.length, passwordConfirmation]);

    const getEmailClassName = () => {
        if (validateEmail(credentials.email) || credentials.email === "") {
            return "";
        } else {
            return "is-invalid";
        }
    };

    const getPasswordConfirmationClassName = () => {
        if (
            passwordConfirmation === credentials.password ||
            passwordConfirmation.length < 8 ||
            credentials.password.length < 8 ||
            credentials.password === ""
        ) {
            return "";
        } else {
            return "is-invalid";
        }
    };

    const getPasswordClassName = () => {
        if (credentials.password.length >= 8 || credentials.password === "") {
            return "";
        } else {
            return "is-invalid";
        }
    };

    const onFormChange = (event): void => {
        event.preventDefault();
        if (event.target.id === "passwordConfirmation") {
            setPasswordConfirmation(event.target.value);
        } else {
            setCredentials({...credentials, [event.target.id]: event.target.value});
        }
    };

    const onSubmit = (event: FormEvent): void => {
        event.preventDefault();
        let state = {};
        registration(credentials).then(result => {
            if (result === true) {
                state = {
                    success: true,
                    message: "Регистрация успешна!"
                }
                history.push("/sign-in", state);
            } else {
                enableAlert(result);
                window.scrollTo(0, 0);
            }
        })
    }

    return (
        <>
            <Alert
                variant="danger"
                show={show}
                onClose={(): void => setShow(false)}
                dismissible
            >
                <div className="p-1">{alertMsg}</div>
            </Alert>
            <Container className={styles.container}>
                <Row className={styles.formRow}>
                    <Form className={`${styles.form} ${styles.inputColumn} p-4`} onChange={onFormChange}
                          onSubmit={onSubmit}>
                        <h4>
                            <Form.Label className="text-secondary">Заполнение медкарты</Form.Label>
                        </h4>

                        <Form.Group className={styles.regInput} controlId="email">
                            <Form.Label>Почта</Form.Label>
                            <Form.Control
                                type="email"
                                placeholder="bolnoi.ivanov@g.nsu.ru"
                                className={getEmailClassName()}
                            />
                            <Form.Label className="invalid-feedback">Это не почта</Form.Label>
                        </Form.Group>

                        <Form.Group className={styles.regInput} controlId="password">
                            <Form.Label>Пароль</Form.Label>
                            <Form.Control
                                type="password"
                                className={getPasswordClassName()}
                            />
                            <Form.Label className="invalid-feedback">
                                Пароль должен быть длиной хотя бы 8 символов
                            </Form.Label>
                        </Form.Group>

                        <Form.Group className={styles.regInput} controlId="passwordConfirmation">
                            <Form.Label>
                                Подтверждение пароля
                            </Form.Label>
                            <Form.Control
                                type="password"
                                className={getPasswordConfirmationClassName()}
                            />
                            <Form.Label className="invalid-feedback">
                                Пароли не совпадают
                            </Form.Label>
                        </Form.Group>

                        <Form.Group className={styles.regInput} controlId="firstName">
                            <Form.Label>Ваше имя</Form.Label>
                            <Form.Control type="text" placeholder="Алексей"/>
                        </Form.Group>

                        <Form.Group className={styles.regInput} controlId="lastName">
                            <Form.Label>Ваша фамилия</Form.Label>
                            <Form.Control type="text" placeholder="Голубев"/>
                        </Form.Group>

                        <Form.Group className={styles.regInput} controlId="contactInfo">
                            <Form.Label>Контактные данные</Form.Label>
                            <Form.Control type="text" placeholder="Например, vk.com/doorka, tg: doorka"/>
                        </Form.Group>

                        <Form.Group className={styles.regInput} controlId="isInAcadem">
                            <Form.Label>Вы в академе?</Form.Label>
                            <Form.Control as="select">
                                <option value="true">Да</option>
                                <option value="false">Нет</option>
                            </Form.Control>
                        </Form.Group>

                        <Form.Group>
                            <Button
                                type="submit"
                                className={`${styles.submitButton} btn-success`}
                                disabled={!canSubmit}
                            >
                                Завести карту
                            </Button>
                            <Row className={styles.signIn}>
                                <h6 className={styles.secondaryText}>Уже завели карту?</h6>
                                <h6 className={styles.link}>
                                    <Link to="/sign-in">Входите</Link>
                                </h6>
                            </Row>
                        </Form.Group>
                    </Form>
                </Row>
            </Container>
        </>
    );
}
