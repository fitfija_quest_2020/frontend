import React, {useEffect, useState} from "react";
import {AlertMessage, ItemDto} from "../../utils/interfaces";
import {Card, Col, Figure, ListGroup, Row, Button, Table} from "react-bootstrap";
import styles from "../../css/eandn.module.css";
import {buyItem, getShop} from "../../apis/user_api";
import Alert from "react-bootstrap/Alert";

interface ShopProps {
    onPointsUpdate: (arg0: number) => void;
}
export default function Shop(props: ShopProps): JSX.Element {

    const [show, setShow] = useState<boolean>(false);
    const [alertMsg, setAlertMsg] = useState<string>("");
    const [alertVariant, setAlertVariant] = useState<string>("");

    const enableAlert = (alert: AlertMessage): void => {
        if (alert !== undefined && alert.message !== undefined) {
            setAlertMsg(alert.message);
            setShow(true);
            alert.success ? setAlertVariant("success") : setAlertVariant("danger");
        }
    };

    const TABLE_WIDTH = 2;

    const [items, setItems] = useState<ItemDto[]>([]);
    const [loading, setLoading] = useState<boolean>(true);
    useEffect(() => {
        if (loading) {
            setLoading(false);
            getShop().then(result => {
                if (typeof result !== 'string') {
                    setItems(result as ItemDto[]);
                }
            })
        }
    })

    const onClick = (event) => {
        const id = parseInt(event.currentTarget.id);
        buyItem(id).then(result => {
            if (typeof result !== 'string') {
                setItems(items.map(item => item.id === id ? {...item, inStock: item.inStock - 1} : item));
                props.onPointsUpdate(items.find(item => item.id === id).price  * -1);
                enableAlert({success: true, message: `Вы успешно купили ${items.find(item => item.id === id).name}`});
            } else {
                enableAlert({success: false, message: result as string});
            }
        });
    }

    return (
        <>
            <Alert
                variant={alertVariant}
                show={show}
                onClose={(): void => setShow(false)}
                dismissible
            >
                <div className="p-1">{alertMsg}</div>
            </Alert>
            <Row className={styles.row}>
                <h2 className="text-success">Магазин</h2>
            </Row>
            <Table borderless className={`mt-4 ${styles.item}`}>
                <tbody>
                {items.reduce((accum, item, index) => {
                    if (index % TABLE_WIDTH === 0) {
                        accum.push([]);
                    }
                    const newArray = accum.pop();
                    newArray.push(item);
                    accum.push(newArray);
                    return accum;
                }, []).map((itemRow) => {
                    return <tr>
                        {
                            itemRow.map(singleItem => {
                                return (<td id={`item-${singleItem.id}`}>
                                        <Card className={styles.card}>
                                            <Row>
                                                <Col sm="auto">
                                                    <Figure className="m-2">
                                                        <Figure.Image
                                                            width={180}
                                                            height={180}
                                                            alt="180x180"
                                                            src={singleItem.imageUrl}
                                                        />
                                                    </Figure>
                                                </Col>
                                                <Col>
                                                    <Card.Body>
                                                        <Card.Title
                                                            className="text-success">{singleItem.name}</Card.Title>
                                                        <Card.Subtitle
                                                            className="mb-2 text-muted">{`В наличии: ${singleItem.inStock}. Цена: ${singleItem.price}`}</Card.Subtitle>
                                                        <Card.Text className={styles.overflow}>
                                                            {singleItem.description}
                                                        </Card.Text>
                                                        <Card.Footer className={styles.footer}>
                                                            <Button id={singleItem.id} variant="outline-success" onClick={onClick}>
                                                                Купить
                                                            </Button>
                                                        </Card.Footer>
                                                    </Card.Body>
                                                </Col>
                                            </Row>
                                        </Card>
                                    </td>
                                )
                            })
                        }
                    </tr>

                })}
                </tbody>
            </Table>
        </>
    )
}
