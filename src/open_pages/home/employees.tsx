import React, {useEffect, useState} from "react";
import {EmployeeDto} from "../../utils/interfaces";
import {getEmployees} from "../../apis/home_api";
import {Card, Col, Figure, ListGroup, Row} from "react-bootstrap";
import styles from "../../css/eandn.module.css";

export default function Employees(): JSX.Element {

    const [employees, setEmployees] = useState<EmployeeDto[]>([]);
    const [loading, setLoading] = useState<boolean>(true);
    useEffect(() => {
        if (loading) {
            setLoading(false);
            getEmployees().then(result => {
                if (typeof result !== 'string') {
                    setEmployees(result as EmployeeDto[]);
                }
            })
        }
    })


    return (<>
            <Row className={styles.row}>
                <h2 className="m-2 text-success">Наши сотрудники</h2>
            </Row>
            <ListGroup className="mt-4">
                {employees.map(employee => {
                    return (<ListGroup.Item id={`employee-${employee.id}`}>
                        <Card>
                            <Card.Body>
                                <Row>
                                    <Col sm="auto">
                                        <Figure>
                                            <Figure.Image
                                                width={180}
                                                height={180}
                                                alt="180x180"
                                                src={employee.imageUrl}
                                            />
                                        </Figure>
                                    </Col>
                                    <Col>
                                        <Card.Title className="text-success">{employee.name}</Card.Title>
                                        <Card.Subtitle className="mb-2 text-muted">{employee.position}</Card.Subtitle>
                                        <Card.Text className={styles.overflow}>
                                            {employee.description}
                                        </Card.Text>
                                    </Col>
                                </Row>
                            </Card.Body>
                        </Card>
                    </ListGroup.Item>)
                })}
            </ListGroup>
        </>
    )
}
