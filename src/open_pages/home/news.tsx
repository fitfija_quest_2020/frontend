import React, {useEffect, useState} from "react";
import {NewsDto} from "../../utils/interfaces";
import {getNews} from "../../apis/home_api";
import {Card, ListGroup, Row} from "react-bootstrap";
import styles from "../../css/eandn.module.css";

export default function News(): JSX.Element {

    const [news, setNews] = useState<NewsDto[]>([]);
    const [loading, setLoading] = useState<boolean>(true);
    useEffect(() => {
        if (loading) {
            setLoading(false);
            getNews().then(result => {
                if (typeof result !== 'string') {
                    setNews(result as NewsDto[]);
                }
            })
        }
    })


    return (
        <>
            <Row className={styles.row}>
                <h1 className="mb-2 title text-secondary text-lg-center">Добро пожаловать на сайт
                    психбольницы №241!</h1>
            </Row>
            <Row className={styles.row}>
                <h2 className="text-success">Последние новости</h2>
            </Row>
            <ListGroup className="mt-4">
                {news.map(singleNews => {
                    return (<ListGroup.Item id={`news-${singleNews.id}`}>
                        <Card>
                            <Card.Body>
                                <Card.Title className="text-success">{singleNews.headline}</Card.Title>
                                <Card.Subtitle className="mb-2 text-muted">{singleNews.created}</Card.Subtitle>
                                <Card.Text className={styles.overflow}>
                                    {singleNews.text}
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </ListGroup.Item>)
                })}
            </ListGroup>
        </>
    )
}
