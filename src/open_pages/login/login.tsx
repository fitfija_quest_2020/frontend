import React, {FormEvent, useEffect, useState} from "react";
import {Button, Col, Container, Form, Row} from "react-bootstrap";

import {login} from "../../apis/auth_api";
import {Link, useHistory} from "react-router-dom";
import styles from "../../css/login.module.css"
import Alert from "react-bootstrap/Alert";
import {AlertMessage, LoginCredentials} from "../../utils/interfaces";
import {isEnabled} from "../../apis/user_api";

export default function SignIn(): JSX.Element {

    // alert part
    const [show, setShow] = useState<boolean>(false);
    const [alertMsg, setAlertMsg] = useState<string>("");
    const [alertVariant, setAlertVariant] = useState<string>("");

    const enableAlert = (alert: AlertMessage): void => {
        if (alert !== undefined && alert.message !== undefined) {
            setAlertMsg(alert.message);
            setShow(true);
            alert.success ? setAlertVariant("success") : setAlertVariant("danger");
        }
    };
    // end of alert part

    const [isRegistrationEnabled, setRegistrationEnabled] = useState<boolean>(true);
    const [credentials, setCredentials] = React.useState<LoginCredentials>({
        password: "",
        email: ""
    });
    const history = useHistory<AlertMessage>();

    const onSubmit = async (event: FormEvent) => {
        event.preventDefault();
        login(credentials).then(result => {
            if (result === true) {
                history.push("/");
            } else {
                const failAlert = {
                    success: false,
                    message: result as string,
                };
                enableAlert(failAlert);
            }
        });
    };
    const alert = history.location.state;

    useEffect(() => {
        enableAlert(alert);
        isEnabled("register").then(result => {
            setRegistrationEnabled(result);
        });
    }, [alert]);

    const onFormChange = (event): void => {
        setCredentials({...credentials, [event.target.id]: event.target.value});
    };

    return (
        <>
            <Alert
                variant={alertVariant}
                show={show}
                onClose={(): void => setShow(false)}
                dismissible
            >
                <div className="p-1">{alertMsg}</div>
            </Alert>
            <Container className={styles.container}>
                <Row className={`${styles.formRow} flex-shrink`}>
                    <Col md="auto" className={`p-2 ${styles.inputColumn}`}>
                        <Form
                            className={styles.form}
                            onSubmit={onSubmit}
                            onChange={onFormChange}
                        >
                            <h3 className="mb-4 text-secondary">Психдиспансер №241</h3>
                            <Form.Group className={styles.input} controlId="email">
                                <Form.Label>Почта</Form.Label>
                                <Form.Control placeholder="Введите почту"/>
                            </Form.Group>

                            <Form.Group className={styles.input} controlId="password">
                                <Form.Label>Пароль</Form.Label>
                                <Form.Control
                                    type="password"
                                    placeholder="Введите пароль"
                                />
                            </Form.Group>

                            <Form.Group controlId="submit-btn">
                                <Button
                                    className={`${styles.submitButton} btn-success`}
                                    type="submit"
                                >
                                    В дурку
                                </Button>
                            </Form.Group>

                            <Form.Group className={styles.signUp} controlId="help-btns" hidden={!isRegistrationEnabled}>
                                <Link
                                    to="/sign-up"
                                    className={`${styles.link} float-right`}
                                >
                                    Записаться
                                </Link>
                            </Form.Group>
                        </Form>
                    </Col>
                </Row>
            </Container>
        </>
    );
}
