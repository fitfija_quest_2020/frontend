import React, {useEffect, useState} from "react";
import {LeaderboardEntry} from "../../utils/interfaces";
import {getLeaderboard} from "../../apis/user_api";
import {ListGroup, Row} from "react-bootstrap";
import styles from "../../css/leaderboard.module.css";

export default function Leaderboard() {

    const [leaderboard, setLeaderboard] = useState<LeaderboardEntry[]>([]);
    const [loading, setLoading] = useState<boolean>(true);

    useEffect(() => {
        if (loading) {
            setLoading(false);
            getLeaderboard().then(lb => {
                if (typeof lb !== "string") {
                    setLeaderboard(lb as LeaderboardEntry[]);
                }
            })
        }
    }, [loading])

    return (<div className={styles.container}>
        <Row className={`mb-2 ${styles.row}`}>
            <h2 className="text-success">Таблица лидеров</h2>
        </Row>
        <Row className={styles.row}>
            <ListGroup className={`${styles.card}`}>
                {leaderboard.map((lb, index) => {
                    return <ListGroup.Item className={styles.center}>{`${index + 1}) ${lb.name}: ${lb.points} очков`}</ListGroup.Item>
                })}
            </ListGroup>
        </Row>
    </div>)
}
