import React, {useEffect, useState} from "react";
import {Button, Navbar, Row, Image, Col, NavDropdown} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";
import {useRouteMatch, useHistory, Link} from "react-router-dom";
import "../css/home.css";
import styles from "../css/home.module.css";
import {logout} from "../apis/auth_api";
import Switch from "react-bootstrap/Switch";
import PrivateRoute from "../utils/private_route";
import {ErrorBody, User, NotificationDto, TeamDto} from "../utils/interfaces";
import {currentTeam, currentUser, getNotifications, isEnabled, readNotification} from "../apis/user_api";
import Profile from "./profile/profile";
import image from "../pics/2.png";
import Stage from "./stage/stage";
import {toast} from "react-toastify";
import News from "./home/news";
import Employees from "./home/employees";
import Leaderboard from "./leaderboard/leaderboard";
import Shop from "./home/shop";

export default function Home(): JSX.Element {
    const {url} = useRouteMatch();
    const history = useHistory();
    const [user, setUser] = useState<User>({
        id: 0,
        email: "",
        invitationId: 0,
        points: 0,
        firstName: "",
        lastName: "",
        faculty: "",
        contactInfo: "",
        isInAcadem: false
    });
    const [loading, isLoading] = useState<boolean>(true);
    const [notifications, setNotifications] = useState<NotificationDto[]>([]);
    const [shopEnabled, setShowEnabled] = useState<boolean>(false);
    const [leaderboardEnabled, setLeaderboardEnabled] = useState<boolean>(false);
    const [teamsEnabled, areTeamsEnabled] = useState<boolean>(false);
    const [isTeamEnableLoaded, setTeamEnableLoaded] = useState<boolean | "ready">(false);
    const [team, setTeam] = useState<TeamDto>({
        created: "",
        currentPoints: 0,
        id: -1,
        name: "",
        players: [],
        totalPoints: 0,
        type: "",
        updated: ""
    });

    const addPoints = (newPoints: number): void => {
        if (teamsEnabled) {
            setTeam({...team, currentPoints: team.currentPoints + newPoints})
        } else {
            setUser({...user, points: user.points + newPoints})
        }
    };

    const refreshNotifications = () => {
        getNotifications().then(result => {
            if (typeof result !== "string") {
                const notificationResult = result as NotificationDto[];
                const newNotifications = notificationResult.filter(not => !toast.isActive(not.id));
                setNotifications(notifications.concat(newNotifications));
                newNotifications.filter(not => !toast.isActive(not.id)).forEach(not => {
                    toast.success(not.text, {
                        toastId: not.id,
                        autoClose: false,
                        className: styles.toast,
                        onClose: () => {
                            readNotification(not.id);
                        }
                    });
                })
            }
        })
    }

    useEffect(() => {
        if (loading) {
            isLoading(false);
            setInterval(refreshNotifications, 30000);
            isEnabled("shop").then(setShowEnabled);
            isEnabled("leaderboard").then(setLeaderboardEnabled);
            isEnabled("teams").then(result => {
                areTeamsEnabled(result);
                if (result === true) {
                    setTeamEnableLoaded("ready");
                } else {
                    setTeamEnableLoaded(true);
                }
            })
            currentUser().then(gotUser => {
                if ((gotUser as ErrorBody).error === undefined) {
                    setUser(gotUser as User);
                } else {
                    logout();
                    history.push("/sign-in", {
                        success: false,
                        message: (gotUser as ErrorBody).error
                    });
                }
            });
        }
        if (isTeamEnableLoaded === "ready") {
            setTeamEnableLoaded(true);
            currentTeam().then(result => {
                if (typeof result !== "string") {
                    setTeam(result as TeamDto);
                } else {
                    if (result !== "У пациента нет команды, но для этого действия она должна быть!") {
                        logout();
                        history.push("/sign-in", {
                            success: false,
                            message: result as string
                        });
                    }
                }
            })
        }
    }, [notifications, loading, history, isTeamEnableLoaded]);

    const onLogoutClick = (event): void => {
        logout();
        history.push("/");
        event.preventDefault();
    };

    const userName = (): string => {
        if (loading) {
            return "";
        } else {
            return `${user.firstName} ${user.lastName}`;
        }
    }

    return (
        <div>
            <Navbar bg="light" expand="lg">
                <Link className={styles.image} to={`${url}`}>
                    <Image src={image}/>
                </Link>
                <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                <Navbar.Collapse className="justify-content-end">
                    <Col md="auto">
                        <NavDropdown className={styles.navLink} title="Отделения" id="dropdown-links">
                            <LinkContainer to={`${url}/profile`}>
                                <NavDropdown.Item className={styles.navItem}>
                                    Личная палата
                                </NavDropdown.Item>
                            </LinkContainer>
                            <LinkContainer to={`${url}/tasks`}>
                                <NavDropdown.Item className={styles.navItem}>
                                    Задания
                                </NavDropdown.Item>
                            </LinkContainer>
                            <LinkContainer to={`${url}`}>
                                <NavDropdown.Item className={styles.navItem}>
                                    Новости
                                </NavDropdown.Item>
                            </LinkContainer>
                            <LinkContainer to={`${url}/employees`}>
                                <NavDropdown.Item className={styles.navItem}>
                                    Персонал
                                </NavDropdown.Item>
                            </LinkContainer>
                            <NavDropdown.Item className={styles.navItem} disabled={!shopEnabled}
                                              href={`${url}/shop`}>Магазин</NavDropdown.Item>
                            <NavDropdown.Item className={styles.navItem} disabled={!leaderboardEnabled}
                                              href={`${url}/leaderboard`}>Лучшие шизы</NavDropdown.Item>
                        </NavDropdown>
                    </Col>
                    <Col sm="auto">
                        <Row className={`${styles.name} ${styles.profileRow}`}>{userName()}</Row>
                        <Row
                            className={styles.profileRow}>{`Баллы: ${teamsEnabled ? team.currentPoints : user.points}`}</Row>
                        <Row className={teamsEnabled ? styles.profileRow : "d-none"}>{`${team.name}`}</Row>
                        <Row className={styles.profileRow}>
                            <Button size="sm" className="btn-success" onClick={onLogoutClick}>
                                Выйти покурить
                            </Button>
                        </Row>
                    </Col>
                </Navbar.Collapse>
            </Navbar>
            <Switch>
                <PrivateRoute path={`${url}/leaderboard`} component={Leaderboard} exact/>
                <PrivateRoute path={`${url}/tasks/:taskId`} exact>
                    <Stage onPointsUpdate={addPoints}/>
                </PrivateRoute>
                <PrivateRoute path={`${url}/tasks/`} exact>
                    <Stage onPointsUpdate={addPoints}/>
                </PrivateRoute>
                <PrivateRoute path={`${url}/profile`} exact>
                    <Profile shopEnabled={shopEnabled}/>
                </PrivateRoute>
                <PrivateRoute path={`${url}`} component={News} exact/>
                <PrivateRoute path={`${url}/employees`} component={Employees} exact/>
                <PrivateRoute path={`${url}/shop`}exact>
                    <Shop onPointsUpdate={addPoints}/>
                </PrivateRoute>
            </Switch>
        </div>
    );
}
