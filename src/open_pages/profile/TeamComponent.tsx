import React, {useEffect, useState} from "react";
import {TeamDto, User} from "../../utils/interfaces";
import {currentTeam, updateTeam} from "../../apis/user_api";
import styles from "../../css/profile.module.css";
import {Button, Col, FormControl, InputGroup, Row} from "react-bootstrap";

interface TeamComponentProps {
    killLogin: (arg0: string) => void;
    myUserId: number;
    enableAlert: (arg0) => void;
}

export default function TeamComponent(props: TeamComponentProps) {
    const [loading, setLoading] = useState<boolean>(true);
    const [team, setTeam] = useState<TeamDto>({
        created: "",
        currentPoints: 0,
        id: -1,
        name: "",
        players: [],
        totalPoints: 0,
        type: "",
        updated: ""
    });
    const [secondPlayer, setSecondPlayer] = useState<User>({
        contactInfo: "",
        email: "",
        faculty: "",
        firstName: "",
        id: -1,
        invitationId: 0,
        isInAcadem: false,
        lastName: "",
        points: 0
    });
    const [teamName, setTeamName] = useState<string>("");

    const onChangeTeamName = (event) => {
        setTeamName(event.target.value);
    }

    const submitTeamName = () => {
        updateTeam(teamName).then(result => {
            if (result === true) {
                setTeam({
                    ...team,
                    name: teamName
                })
                props.enableAlert({success: true, message: "Вы успешно изменили название команды!"})
            } else {
                props.enableAlert({success: false, message: result})
            }
        })
    }

    useEffect(() => {
        if (loading) {
            setLoading(false);
            currentTeam().then(team => {
                if (typeof team !== "string") {
                    setTeam(team as TeamDto);
                    setSecondPlayer(team.players.filter(player => player.id !== props.myUserId)[0]);
                    setTeamName(team.name);
                } else {
                    if (team !== "У пациента нет команды, но для этого действия она должна быть!") {
                        props.killLogin(team as string);
                    }
                }
            })
        }
    })

    return (<div>
        <Row className={styles.row}>
            <Col>Название команды:</Col>
            <Col>
                <InputGroup className={`${styles.input}`}>
                <FormControl
                    type="input"
                    placeholder="Название команды"
                    value={teamName}
                    id="newName"
                    onChange={onChangeTeamName}
                />
                <InputGroup.Append>
                    <Button
                        onClick={submitTeamName}
                        variant="outline-success"
                        disabled={teamName.trim() === "" || teamName.length > 50}
                    >
                        Сохранить
                    </Button>
                </InputGroup.Append>
            </InputGroup>
            </Col>
        </Row>
        <Row className={styles.row}>
            <Col>Тип команды:</Col>
            <Col>{team.type}</Col>
        </Row>
        <Row className={styles.row}>
            <Col>Общие баллы команды:</Col>
            <Col>{team.totalPoints}</Col>
        </Row>
        <Row className={styles.row}>
            <Col>Текущие баллы команды:</Col>
            <Col>{team.currentPoints}</Col>
        </Row>
        <Row className={styles.row}>
            <Col>Второй участник:</Col>
            <Col>{`${secondPlayer.firstName} ${secondPlayer.lastName}`}</Col>
        </Row>
        <Row className={styles.row}>
            <Col>Контактные данные второго участника:</Col>
            <Col>{secondPlayer.contactInfo}</Col>
        </Row>

    </div>);
}
