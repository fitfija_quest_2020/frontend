import {Button, Col, ListGroup, Row} from "react-bootstrap";
import {BsFillPersonCheckFill, BsFillPersonDashFill} from "react-icons/all";
import React, {MouseEvent} from "react";

interface InvProps {
    id: string;
    inbox: boolean;
    senderName: string;
    status: string;
    onAccept: (arg0: MouseEvent) => void;
    onReject: (arg0: MouseEvent) => void;
}

export default function InvitationItem(props: InvProps): JSX.Element {
    const who = props.inbox ? "Вас приглашает" : "Вы приглашаете";
    return (<ListGroup.Item className="bg-light">
        <Col md="auto">
            <Row className="m-1">{`${who} ${props.senderName}. Статус: ${props.status}`}</Row>
            <Button id={props.id}
                    onClick={props.onAccept}
                    hidden={!props.inbox}
                    size="sm"
                    className="mr-1"
                    variant="success">
                <BsFillPersonCheckFill />
            </Button>
            <Button id={props.id}
                    onClick={props.onReject}
                    hidden={!props.inbox}
                    size="sm"
                    className="mr-1"
                    variant="danger">
                <BsFillPersonDashFill/>
            </Button>
        </Col>
    </ListGroup.Item>)
}
