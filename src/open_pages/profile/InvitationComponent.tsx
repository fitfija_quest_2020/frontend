import React, {useEffect, useState} from "react";
import {Button, Col, Form, ListGroup, Row, Tab, Tabs} from "react-bootstrap";
import styles from "../../css/profile.module.css";
import InvitationItem from "./InvitationItem";
import {AlertMessage, Invitation} from "../../utils/interfaces";
import {acceptInvitation, getInvitations, rejectInvitation, sendInvitation} from "../../apis/user_api";

interface InvitationComponentProps {
    invitationId: string;
    hasTeam: boolean;
    teammateName: string;
    enableAlert: (arg0: AlertMessage) => void;
    setInviteInfo: (arg0) => void;
}

export default function InvitationComponent(props: InvitationComponentProps) {
    const [incomingInvitations, setIncomingInvitations] = useState<Invitation[]>([]);
    const [outcomingInvitations, setOutcomingInvitations] = useState<Invitation[]>([]);
    const [userId, setUserId] = useState<string>("");
    const [loading, setLoading] = useState<boolean>(true);
    const onFormChange = (event) => {
        setUserId(event.target.value);
    }

    const canPress = (): boolean => {
        return userId.length > 0 && userId.length < 21 && !isNaN(parseInt(userId)) && parseInt(userId).toString() === userId;
    }

    useEffect(() => {
        if (loading) {
            setLoading(false);
            getInvitations(true).then(result => {
                setIncomingInvitations(result as Invitation[]);
            });
            getInvitations(false).then(result => {
                setOutcomingInvitations(result as Invitation[]);
            })
        }
    })


    const onAccept = (event): void => {
        event.preventDefault();
        const id = parseInt(event.currentTarget.id);
        acceptInvitation(id).then(result => {
            if (result === true) {
                const newTeammate = incomingInvitations.find(invite => invite.id === id);
                props.setInviteInfo({
                    hasTeam: true,
                    teammateName: newTeammate.senderName,
                })
            } else {
                props.enableAlert({success: false, message: result})
            }
        })
    }

    const onReject = (event): void => {
        event.preventDefault();
        const id = parseInt(event.currentTarget.id);
        rejectInvitation(id).then(result => {
            if (result === true) {
                setIncomingInvitations(incomingInvitations.filter(inv => inv.id !== id));
            } else {
                props.enableAlert({success: false, message: result})
            }
        })
    }

    const onSendInvitation = (event): void => {
        event.preventDefault();
        sendInvitation(parseInt(userId)).then(result => {
            if (result !== true) {
                props.enableAlert({success: false, message: result});
            } else {
                props.enableAlert({success: true, message: "Приглашение успешно отправлено!"});
                getInvitations(false).then(result => {
                    setOutcomingInvitations(result as Invitation[]);
                });
            }
        });
    }

    return (
        <>
            <Row className={!props.hasTeam ? styles.formRow : "d-none"}>
                <Col>
                    <Form className={styles.form} onSubmit={onSendInvitation} onChange={onFormChange}>
                        <Row className={styles.row}>
                            <Col>Отправить приглашение:</Col>
                            <Col>
                                <Form.Group className="m-0" controlId="id">
                                    <Form.Control className={styles.input} placeholder="Введите номер карты"/>
                                </Form.Group>
                            </Col>
                        </Row>
                        <Row className={styles.row}>
                            <Col>
                            </Col>
                            <Col>
                                <Form.Group controlId="submit-btn">
                                    <Button
                                        size="sm"
                                        className={`${styles.submitButton} btn-success`}
                                        type="submit"
                                        disabled={!canPress()}
                                    >
                                        Позвать в палату
                                    </Button>
                                </Form.Group>
                            </Col>
                        </Row>
                    </Form>
                </Col>
            </Row>
            <Row className={styles.row}>
                Приглашения:
            </Row>
            <div className={styles.row}>
                <Tabs variant="pills" defaultActiveKey="incoming" id="invitations-tab">
                    <Tab tabClassName={"mr-1 mb-1 bg-success text-white " + styles.noOutline}
                         eventKey="incoming"
                         title="Входящие">
                        <ListGroup>
                            {incomingInvitations.map((line) => {
                                return <InvitationItem onAccept={onAccept}
                                                       onReject={onReject}
                                                       status={line.status}
                                                       id={line.id.toString()}
                                                       inbox
                                                       senderName={line.senderName}/>
                            })}
                        </ListGroup>
                    </Tab>
                    <Tab tabClassName={"mb-1 bg-success text-white " + styles.noOutline} eventKey="outcoming"
                         title="Исходящие">
                        <ListGroup>
                            {outcomingInvitations.map((line) => {
                                return <InvitationItem onAccept={onAccept}
                                                       onReject={onReject}
                                                       id={line.id.toString()}
                                                       status={line.status}
                                                       inbox={false}
                                                       senderName={line.receiverName}/>
                            })}
                        </ListGroup>
                    </Tab>
                </Tabs>
            </div>
        </>)
};
