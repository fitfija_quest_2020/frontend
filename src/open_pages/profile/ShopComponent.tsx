import React, {useEffect, useState} from "react";
import {PurchaseDto} from "../../utils/interfaces";
import {getPurchases} from "../../apis/user_api";
import styles from "../../css/profile.module.css";
import {ListGroup, Row} from "react-bootstrap";

// @ts-ignore
export default function ShopComponent(): JSX.Element {
    const localisation = {"IN_TRANSIT": "Передано в доставку", "DELIVERED": "Доставлено", "CANCELED": "Заказ отменен"}

    const [purchase, setPurchases] = useState<PurchaseDto[]>([]);
    const [loading, setLoading] = useState<boolean>(true);
    useEffect(() => {
        if (loading) {
            setLoading(false);
            getPurchases().then(result => {
                if (typeof result !== 'string') {
                    setPurchases(result as PurchaseDto[]);
                }
            })
        }
    })

    return (<div className={purchase.length === 0 ? 'd-none' : ""}>
        <Row className={styles.row}>
            <h3 className="text-success">Ваши заказы</h3>
        </Row>
        <ListGroup>
            {purchase.map(entry => {
                return (<ListGroup.Item className={`${styles.bgGrad} ${styles.row}`}>
                    {`${entry.item.name}: куплено ${entry.created}. Статус: ${localisation[entry.status]}`}
                </ListGroup.Item>)
            })}
        </ListGroup>
    </div>);
}
