import React, {useEffect, useState} from "react";
import {useHistory} from "react-router-dom";
import {
    AlertMessage,
    ErrorBody,
    InvitationInfoDto,
    User,
    UserUpdate
} from "../../utils/interfaces";
import styles from "../../css/profile.module.css";
import {
    currentUser,
    updateUser,
    isEnabled,
    invitationInfo,
    changePassword
} from "../../apis/user_api";
import {logout} from "../../apis/auth_api";
import {
    Button,
    Col,
    Container, Dropdown,
    Form, FormControl, InputGroup,
    Jumbotron,
    Row
} from "react-bootstrap";
import Alert from "react-bootstrap/Alert";
import InvitationComponent from "./InvitationComponent";
import TeamComponent from "./TeamComponent";
import ShopComponent from "./ShopComponent";

type LoadingProgress = "not started" | "loading" | true;

interface ChangePassword {
    newPassword: string;
    newPasswordConfirmation: string;
    oldPassword: string;
}

interface ProfileProps {
    shopEnabled: boolean;
}

export default function Profile(props: ProfileProps): JSX.Element {
    const [alertVariant, setAlertVariant] = useState<string>("");
    const [alertMsg, setAlertMsg] = useState<string>("");
    const [show, setShow] = useState<boolean>(false);
    const [changePasswordCreds, setChangePasswordCreds] = useState<ChangePassword>({
        newPassword: "",
        newPasswordConfirmation: "",
        oldPassword: ""
    })
    const [inviteInfo, setInviteInfo] = useState<InvitationInfoDto>({
        hasTeam: false,
        teammateName: ""
    });
    const [teamsEnableLoaded, isTeamsEnableLoaded] = useState<boolean>(false);
    const [teamsEnabled, areTeamsEnabled] = useState<boolean>(false);
    const [invitationsEnabled, isInvitationsEnabled] = useState<boolean>(false);
    const [isCurrentUserLoaded, setCurrentUserLoaded] = useState<LoadingProgress>("not started");
    const [isInfoLoaded, setInfoLoaded] = useState<LoadingProgress>("not started");
    const [isInvitationEnableLoaded, setInvitationsEnable] = useState<LoadingProgress>("not started");
    const [user, setUser] = useState<User>({
        id: 0,
        email: "",
        invitationId: 0,
        points: 0,
        firstName: "",
        lastName: "",
        faculty: "",
        contactInfo: "",
        isInAcadem: false
    });
    const [shortUser, setShortUser] = useState<UserUpdate>({
        firstName: "",
        lastName: "",
        contactInfo: "",
        isInAcadem: "true"
    })


    const enableAlert = (alert: AlertMessage): void => {
        if (alert !== undefined && alert.message !== undefined) {
            setAlertMsg(alert.message);
            setShow(true);
            alert.success ? setAlertVariant("success") : setAlertVariant("danger");
        }
    };

    const onChangeChangePassword = (event): void => {
        setChangePasswordCreds({...changePasswordCreds, [event.target.id]: event.target.value})
    }

    const onSubmitChangePassword = (): void => {
        changePassword({
            oldPassword: changePasswordCreds.oldPassword, newPassword: changePasswordCreds.newPassword
        }).then(success => {
            if (success === true) {
                enableAlert({message: "Пароль изменен успешно!", success: true})
            } else {
                enableAlert({message: success, success: false})
            }
        });
    }

    const history = useHistory();

    const killLogin = (error: string): void => {
        logout();
        history.push("/sign-in", {
            success: false,
            message: error
        });
    }

    useEffect(() => {
        if (isInfoLoaded === "not started") {
            setInfoLoaded("loading");
            invitationInfo().then(result => {
                if (typeof result !== 'string') {
                    setInviteInfo(result as InvitationInfoDto);
                    setInfoLoaded(true);
                } else {
                    killLogin(result);
                }
            });
        }
        if (isInfoLoaded === true) {
            if (inviteInfo.hasTeam === false) {
                if (isInvitationEnableLoaded === "not started") {
                    setInvitationsEnable("loading")
                    isEnabled("invitations").then(isEnabled => {
                        isInvitationsEnabled(isEnabled);
                        setInvitationsEnable(true)
                    });
                }
            } else {
                setInvitationsEnable(true)
            }
        }
        if (isCurrentUserLoaded === "not started") {
            setCurrentUserLoaded("loading");
            currentUser().then(gotUser => {
                if ((gotUser as ErrorBody).error === undefined) {
                    const newUser = gotUser as User;
                    setUser(newUser);
                    setShortUser({
                        firstName: newUser.firstName,
                        lastName: newUser.lastName,
                        contactInfo: newUser.contactInfo,
                        isInAcadem: newUser.isInAcadem.toString()
                    })
                    setCurrentUserLoaded(true);
                } else {
                    killLogin((gotUser as ErrorBody).error);
                }
            });
        }
        if (!teamsEnableLoaded) {
            isTeamsEnableLoaded(true);
            isEnabled("teams").then(areTeamsEnabled);
        }
    }, [isCurrentUserLoaded,
        isInfoLoaded,
        isInvitationEnableLoaded,
        inviteInfo.hasTeam, history, invitationsEnabled]);


    const shortUserCanSubmit = (): boolean => {
        return shortUser.firstName.length > 0 && shortUser.firstName.length < 15
            && shortUser.lastName.length > 0 && shortUser.firstName.length < 15
            && shortUser.contactInfo.length > 0 && shortUser.contactInfo.length < 50
    }

    const onShortUserChange = (event) => {
        setShortUser({...shortUser, [event.target.id]: event.target.value});
    }

    const onShortUserSubmit = (event) => {
        event.preventDefault();
        updateUser(shortUser).then(result => {
            if (result === true) {
                setUser({
                    ...user,
                    contactInfo: shortUser.contactInfo,
                    firstName: shortUser.firstName,
                    lastName: shortUser.lastName,
                    isInAcadem: shortUser.isInAcadem === "true"
                })
            } else {
                enableAlert({success: false, message: result})
            }
        })
    }


    return (
        <div>
            <Alert
                variant={alertVariant}
                show={show}
                onClose={(): void => setShow(false)}
                dismissible
            >
                <div className="p-1">{alertMsg}</div>
            </Alert>
            <Container fluid="sm" className={styles.container}>
                <Jumbotron>
                    <Row className={styles.row}>
                        <h2 className={`text-success`}>Карта пациента</h2>
                    </Row>
                    <Row className={styles.row}>
                        <Col>Имя пациента:</Col>
                        <Col>{user.firstName}</Col>
                    </Row>
                    <Row className={styles.row}>
                        <Col>Фамилия пациента:</Col>
                        <Col>{user.lastName}</Col>
                    </Row>
                    <Row className={styles.row}>
                        <Col>Контактные данные:</Col>
                        <Col>{user.contactInfo}</Col>
                    </Row>
                    <Row className={styles.row}>
                        <Col>Вы в академе?</Col>
                        <Col>{user.isInAcadem ? "Да" : "Нет"}</Col>
                    </Row>
                    <Row className={styles.row}>
                        <Col>Отделение:</Col>
                        <Col>{user.faculty}</Col>
                    </Row>
                    <div className={teamsEnabled ? "d-none" : ""}>
                        <Row className={styles.row}>
                            <Col>Очки:</Col>
                            <Col>{user.points}</Col>
                        </Row>
                        <Row className={styles.row}>
                            <Col>Номер медкарты:</Col>
                            <Col>{user.invitationId}</Col>
                        </Row>
                        <Row className={inviteInfo.hasTeam ? styles.row : "d-none"}>
                            <Col>Напарник:</Col>
                            <Col>{inviteInfo.teammateName}</Col>
                        </Row>
                    </div>
                    {teamsEnabled ? <TeamComponent killLogin={killLogin} myUserId={user.id} enableAlert={enableAlert}/> : ""}
                    <Row className="mb-3">
                        <Col>
                            <Dropdown className={styles.dropdown}>
                                <Dropdown.Toggle variant="success" id="dropdown-basic">
                                    Изменить пароль
                                </Dropdown.Toggle>
                                <Dropdown.Menu>
                                    <InputGroup className={`m-3 ${styles.inputDropdown}`}>
                                        <FormControl
                                            type="password"
                                            placeholder="Старый пароль"
                                            id="oldPassword"
                                            onChange={onChangeChangePassword}
                                        />
                                    </InputGroup>
                                    <InputGroup className={`m-3 ${styles.inputDropdown}`}>
                                        <FormControl
                                            type="password"
                                            placeholder="Новый пароль"
                                            id="newPassword"
                                            onChange={onChangeChangePassword}
                                        />
                                    </InputGroup>
                                    <InputGroup className={`m-3 ${styles.inputDropdown}`}>
                                        <FormControl
                                            type="password"
                                            placeholder="Подтверждение пароля"
                                            id="newPasswordConfirmation"
                                            onChange={onChangeChangePassword}
                                        />
                                        <InputGroup.Append>
                                            <Button
                                                onClick={onSubmitChangePassword}
                                                variant="outline-success"
                                                disabled={changePasswordCreds.newPassword === "" ||
                                                changePasswordCreds.newPassword !== changePasswordCreds.newPasswordConfirmation}
                                            >
                                                Отправить
                                            </Button>
                                        </InputGroup.Append>
                                    </InputGroup>
                                </Dropdown.Menu>
                            </Dropdown>
                        </Col>
                        <Col>
                            <Dropdown className={styles.dropdown}>
                                <Dropdown.Toggle variant="success" id="dropdown-basic">
                                    Изменить личные данные
                                </Dropdown.Toggle>
                                <Dropdown.Menu>

                                    <div className="m-3">
                                        <label htmlFor="basic-url">Вы в академе?</label>
                                        <InputGroup className={`${styles.inputDropdown}`}>
                                            <Form.Control id="isInAcadem" as="select" value={shortUser.isInAcadem}
                                                          onChange={onShortUserChange}>
                                                <option value="true">Да</option>
                                                <option value="false">Нет</option>
                                            </Form.Control>
                                        </InputGroup>
                                    </div>
                                    <div className="m-3">
                                        <label htmlFor="basic-url">Имя</label>
                                        <InputGroup className={`${styles.inputDropdown}`}>
                                            <FormControl
                                                type="input"
                                                placeholder="Имя"
                                                id="firstName"
                                                value={shortUser.firstName}
                                                onChange={onShortUserChange}
                                            />
                                        </InputGroup>
                                    </div>
                                    <div className="m-3">
                                        <label htmlFor="basic-url">Фамилия</label>
                                        <InputGroup className={`${styles.inputDropdown}`}>
                                            <FormControl
                                                type="input"
                                                placeholder="Фамилия"
                                                id="lastName"
                                                value={shortUser.lastName}
                                                onChange={onShortUserChange}
                                            />
                                        </InputGroup>
                                    </div>
                                    <div className="m-3">
                                        <label htmlFor="basic-url">Контактные данные</label>
                                        <InputGroup className={`${styles.inputDropdown}`}>
                                            <FormControl
                                                type="input"
                                                placeholder="Контактные данные"
                                                id="contactInfo"
                                                value={shortUser.contactInfo}
                                                onChange={onShortUserChange}
                                            />
                                            <InputGroup.Append>
                                                <Button
                                                    onClick={onShortUserSubmit}
                                                    disabled={!shortUserCanSubmit()}
                                                    variant="outline-success"
                                                >
                                                    Отправить
                                                </Button>
                                            </InputGroup.Append>
                                        </InputGroup>
                                    </div>
                                </Dropdown.Menu>
                            </Dropdown>
                        </Col>
                    </Row>
                    {invitationsEnabled && !inviteInfo.hasTeam ?
                        <InvitationComponent enableAlert={enableAlert} setInviteInfo={setInviteInfo}
                                             invitationId={user.invitationId.toString()} hasTeam={inviteInfo.hasTeam}
                                             teammateName={inviteInfo.teammateName}/> : ""
                    }
                    {props.shopEnabled ? <ShopComponent/> : ""}
                </Jumbotron>
            </Container>
        </div>
    );
}
