import {Button, Container, FormControl, InputGroup, ListGroup, Modal, Row, Image} from "react-bootstrap";
import React, {useEffect, useState} from "react";
import styles from "../../css/stage.module.css"
import {AlertMessage, StageDto, TaskDto} from "../../utils/interfaces";
import {getStage, getTaskById, canAnswer, answer} from "../../apis/task_api";
import ShortTask from "./ShortTask";
import {useParams} from "react-router-dom";
import Countdown from "react-countdown";
import Alert from "react-bootstrap/Alert";

interface AnswerStatus {
    message: string;
    className: string;
}

interface StageProps {
    onPointsUpdate: (arg0: number) => void;
}

export default function Stage(props: StageProps): JSX.Element {

    const [showAlert, setShowAlert] = useState<boolean>(false);
    const [alertMsg, setAlertMsg] = useState<string>("");
    const [alertVariant, setAlertVariant] = useState<string>("");

    const enableAlert = (alert: AlertMessage): void => {
        if (alert !== undefined && alert.message !== undefined) {
            setAlertMsg(alert.message);
            setShowAlert(true);
            alert.success ? setAlertVariant("success") : setAlertVariant("danger");
        }
    };

    const {taskId} = useParams<{ taskId: string }>();

    const [stage, setStage] = useState<StageDto>({
        created: "", end: Date.now().toLocaleString(), id: -1, name: "", start: "", tasks: [], updated: ""
    });
    const [isCanAnswer, setCanAnswer] = useState<boolean>(true);
    const [taskAnswer, setAnswer] = useState<string>("");
    const [loading, isLoading] = useState<boolean>(true);
    const [show, setShow] = useState<boolean>(false);
    const [answerStatus, setAnswerStatus] = useState<AnswerStatus>({className: "d-none", message: ""})
    const [selectedModalTask, setTask] = useState<TaskDto>({
        created: "",
        id: -1,
        isCompleted: false,
        isHidden: false,
        points: 0,
        stageId: 0,
        name: "",
        text: "",
        updated: "",
        imageUrl: ""
    });
    const [startTaskLoaded, isStartTaskLoaded] = useState<boolean>(false);

    const handleClose = () => {
        setAnswerStatus({...answerStatus, className: "d-none"})
        setShow(false)
    };

    const onChangeAnswer = (event) => {
        setAnswer(event.currentTarget.value);
    }

    const onSubmitAnswer = () => {
        answer(selectedModalTask.id, taskAnswer).then(result => {
            if (result === true) {
                setTask({...selectedModalTask, isCompleted: true});
                setStage({
                    ...stage,
                    tasks: stage.tasks.map(task => task.id === selectedModalTask.id ? {
                        ...task,
                        isCompleted: true
                    } : task)
                })
                setAnswerStatus({className: "text-success", message: "Правильный ответ!"})
                props.onPointsUpdate(selectedModalTask.points);
            } else {
                setAnswerStatus({className: "text-danger", message: result})
            }
        })
    }

    const loadTaskAsync = (taskId: number): void => {
        getTaskById(taskId as number).then(result => {
            if (typeof result !== 'string') {
                setTask(result as TaskDto);
                setShow(true);
            } else {
                enableAlert({message: result as string, success: false})
            }
        })
    }

    const handleShow = (event): void => {
        const taskId = parseInt(event.currentTarget.id) as number;
        if (selectedModalTask.id === taskId) {
            setShow(true);
        } else {
            loadTaskAsync(taskId);
        }
    }

    useEffect(() => {
        if (loading) {
            isLoading(false);
            getStage().then(result => {
                if (typeof result !== 'string') {
                    setStage(result);
                }
            })
            canAnswer().then(result => {
                if (result === true) {
                    setCanAnswer(true);
                } else {
                    setCanAnswer(false);
                }
            })
        }

        if (!startTaskLoaded) {
            if (taskId != null) {
                loadTaskAsync(parseInt(taskId));
            }
            isStartTaskLoaded(true);
        }
    }, [startTaskLoaded, taskId, loading])

    const renderTasks = (): JSX.Element => {
        const filteredTasks = stage.tasks.filter(task => !task.isHidden);
        const groupedTasks = [[]];
        filteredTasks.forEach(task => {
            let lastGroup = groupedTasks.pop();
            if (lastGroup.length === 3) {
                groupedTasks.push(lastGroup, [task])
            } else {
                lastGroup.push(task);
                groupedTasks.push(lastGroup);
            }
        });
        return (<Row className={styles.row}>{groupedTasks.map(group =>
                <ListGroup horizontal>
                    {group.map(task => <ShortTask id={task.id.toString()}
                                                  name={task.name}
                                                  isCompleted={task.isCompleted}
                                                  points={task.points}
                                                  handleShow={handleShow}/>)}
                </ListGroup>)}
            </Row>
        );
    }

    return (
        <div>
            <Alert
                variant={alertVariant}
                show={showAlert}
                onClose={(): void => setShowAlert(false)}
                dismissible
            >
                <div className="p-1">{alertMsg}</div>
            </Alert>
            <Container className={styles.container}>
                <Row className={styles.row}>
                    <h2 className="text-success">{stage.name}</h2>
                </Row>
                <Row className={styles.row}>
                    <Countdown key={stage.end} className="h1 text-success" date={Date.parse(stage.end)}>
                        <span className="h1 text-success">Этап окончен!</span>
                    </Countdown>
                </Row>
                <Row className={styles.row}>
                    Текущие задания:
                </Row>
                {renderTasks()}
            </Container>
            <Modal centered size="lg" show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title className="text-success">{selectedModalTask.name}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Container>
                        <div className={styles.overflow}>
                            {selectedModalTask.text}
                        </div>
                        <Row className={`m-2 ${
                            selectedModalTask.imageUrl === null || selectedModalTask.imageUrl === "" ? "d-none" : ""
                        }`}>
                            <Image src={selectedModalTask.imageUrl} thumbnail/>
                        </Row>
                    </Container>
                </Modal.Body>
                <Modal.Footer>
                    <InputGroup className={`mb-3 ${isCanAnswer ? "" : "d-none"}`}>
                        <FormControl
                            onChange={onChangeAnswer}
                            placeholder="Ваш ответ"
                            aria-label="Ваш ответ"
                            aria-describedby="basic-addon2"
                        />
                        <InputGroup.Append>
                            <Button variant="outline-success"
                                    onClick={onSubmitAnswer}
                                    disabled={selectedModalTask.isCompleted || !isCanAnswer}
                            >
                                Отвечаю
                            </Button>
                        </InputGroup.Append>
                    </InputGroup>
                    <Row className="m-2 text-secondary">{`Баллов за задание: ${selectedModalTask.points}`}</Row>
                    <Row className={answerStatus.className}>
                        {answerStatus.message == null ? "Ответ неверный" : answerStatus.message}
                    </Row>
                    <Row className={`text-success ${!selectedModalTask.isCompleted ? "d-none" : ""}`}>
                        Задание выполнено!
                    </Row>
                </Modal.Footer>
            </Modal>
        </div>)
}
