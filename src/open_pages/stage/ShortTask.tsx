import {Button, Card, Col, ListGroup, Row} from "react-bootstrap";
import React, {MouseEvent} from "react";
import { FcCheckmark} from "react-icons/all";
import styles from "../../css/task.module.css"

interface TaskProps {
    id: string;
    name: string;
    points: number;
    handleShow: (arg0: MouseEvent) => void;
    isCompleted: boolean;
}


export default function ShortTask(props: TaskProps): JSX.Element {
    return (
        <ListGroup.Item id={props.id} variant="secondary" className="m-2">
            <Card className="text-center" style={{width: '18rem'}}>
                <Card.Header className={styles.header}>
                    <Card.Title className="text-dark">{props.name}</Card.Title>
                </Card.Header>
                <Card.Body>
                    {props.isCompleted ?
                        <Card.Text className="text-success">
                            <span className="mr-1">Задание выполнено</span>
                            <FcCheckmark/>
                        </Card.Text>
                        :
                        <Card.Text className="text-secondary">
                            Задание не выполнено
                        </Card.Text>
                    }
                </Card.Body>
                <Card.Footer className={styles.header}>
                    <Row>
                        <Col className="mt-1">{`${props.points} очков`}</Col>
                        <Col>
                            <Button id={props.id} variant="success" onClick={props.handleShow}>
                                Смотреть
                            </Button>
                        </Col>
                    </Row>
                </Card.Footer>
            </Card>
            {/*<Row>{`${props.name} за ${props.points}`}</Row>*/}
            {/*<Row>*/}
            {/*    <Button id={props.id} variant="success" onClick={props.handleShow} disabled={!props.canAnswer}>*/}
            {/*        Посмотреть информацию о задании*/}
            {/*    </Button>*/}
            {/*</Row>*/}
        </ListGroup.Item>)
}
