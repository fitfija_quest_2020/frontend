import React, {useEffect, useState} from "react";
import {Switch, Redirect} from "react-router-dom";
import SignIn from "./open_pages/login/login";
import Registration from "./open_pages/registration/registration";
import PrivateRoute from "./utils/private_route";
import UnloggedRoute from "./utils/unlogged_route";
import Home from "./open_pages/home";
import {ToastContainer} from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';
import Footer from 'rc-footer';
import 'rc-footer/assets/index.css';
import styles from './css/home.module.css'
import {isEnabled} from "./apis/user_api";
import "./css/App.css"

function App(): JSX.Element {

    const [isRegistrationEnabled, setRegistrationEnabled] = useState<boolean>(true);
    const [loading, setLoading] = useState<boolean>(true);

    useEffect(() => {
        if (loading) {
            setLoading(false);
            isEnabled("register").then(result => {
                setRegistrationEnabled(result);
            });
        }
    })

    return (
        <div>
            <ToastContainer
                position="bottom-right"
                autoClose={5000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
            <Switch>
                <UnloggedRoute path="/sign-in" component={SignIn} exact/>
                <UnloggedRoute path="/sign-up" component={() => {
                    if (isRegistrationEnabled)
                        return <Registration/>;
                    else
                        return <Redirect to="/"/>;
                }} exact/>
                <PrivateRoute path="/home" component={Home}/>
                <Redirect from="/" to="/sign-in" exact/>
            </Switch>
            <Footer backgroundColor="transparent" columns={[
                {
                    title: "",
                    items: [{title: "По всем вопросам", description: "vk.com/fitfija2020", className: styles.text}],
                    className: styles.text
                }
            ]}/>
        </div>
    );
}

export default App;
