import {
    Confirmation,
    ErrorBody,
    Stage,
    StageDto,
    Task,
    TaskAnswerConfirmationBody,
    TaskDto
} from "../utils/interfaces";
import api from "./api";
import {confimationHandler} from "./user_api";

export async function getStage(): Promise<Stage> {
    return api().currentStage().then(result => {
        const {code, response} = result;
        if (code === 200) {
            return response as StageDto;
        }
        return (response as ErrorBody).error;
    })
}

export async function getTaskById(id: number): Promise<Task> {
    return api().getTaskById(id).then(result => {
        const {code, response} = result;
        if (code === 200) {
            return response as TaskDto;
        }
        return (response as ErrorBody).error;
    })
}

export async function canAnswer(): Promise<Confirmation> {
    return api().canAnswer().then(result => {
        const {response} = result;
        const helpMe = response as any;
        if (helpMe.result === 'true') {
            return true;
        } else {
            return helpMe.result;
        }
    });
}

export async function answer(id: number, answer: string): Promise<Confirmation> {
    return api().answer(id, answer).then(resp => {
        if (resp.code === 200) {
            const {result} = resp.response as TaskAnswerConfirmationBody;
            if (result === 'CORRECT') {
                return true;
            }
            return "Неверный ответ.";
        }
        return (resp.response as ErrorBody).error;
    });
}
