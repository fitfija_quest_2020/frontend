import axios, {AxiosError, AxiosRequestConfig, AxiosResponse} from "axios";
import {getURL} from "../utils/utils";
import {
    ApiResponse,
    LoginCredentials,
    ErrorBody,
    LoginResponse,
    RegistrationCredentials,
    ConfirmationResponse,
    UserResponse,
    UserUpdate,
    EnabledResponse,
    InvitationsResponse,
    InvitationInfoResponse,
    StageResponse,
    TaskResponse,
    TaskAnswerConfirmation,
    NotificationsResponse,
    ChangePasswordCredentials,
    NewsResponse,
    EmployeeResponse,
    TeamResponse, LeaderboardResponse, ItemResponse, PurchaseResponse
} from "../utils/interfaces";
import Cookies from "js-cookie";

export interface ApiClient {
    login(credentials: LoginCredentials): Promise<ApiResponse<LoginResponse>>;

    registration(credentials: RegistrationCredentials): Promise<ApiResponse<ConfirmationResponse>>

    currentUser(): Promise<ApiResponse<UserResponse>>

    updateUser(user: UserUpdate): Promise<ApiResponse<ConfirmationResponse>>

    checkIfEnabled(what: string): Promise<ApiResponse<EnabledResponse>>

    sendInvitation(invitationID: number): Promise<ApiResponse<ConfirmationResponse>>

    getInvitations(incoming: boolean): Promise<ApiResponse<InvitationsResponse>>

    acceptInvitation(id: number): Promise<ApiResponse<ConfirmationResponse>>

    rejectInvitation(id: number): Promise<ApiResponse<ConfirmationResponse>>

    invitationInfo(): Promise<ApiResponse<InvitationInfoResponse>>

    currentStage(): Promise<ApiResponse<StageResponse>>

    getTaskById(id: number): Promise<ApiResponse<TaskResponse>>

    canAnswer(): Promise<ApiResponse<ConfirmationResponse>>

    answer(id: number, answer: string): Promise<ApiResponse<TaskAnswerConfirmation>>

    getNotifications(): Promise<ApiResponse<NotificationsResponse>>

    readNotification(id: number): Promise<ApiResponse<ConfirmationResponse>>

    changePassword(cred: ChangePasswordCredentials): Promise<ApiResponse<ConfirmationResponse>>

    getNews(): Promise<ApiResponse<NewsResponse>>

    getEmployees(): Promise<ApiResponse<EmployeeResponse>>

    getTeam(): Promise<ApiResponse<TeamResponse>>

    changeTeamName(newName: string): Promise<ApiResponse<ConfirmationResponse>>

    getLeaderboard(): Promise<ApiResponse<LeaderboardResponse>>

    getItems(): Promise<ApiResponse<ItemResponse>>

    buyItem(id: number): Promise<ApiResponse<ConfirmationResponse>>

    getPurchases(): Promise<ApiResponse<PurchaseResponse>>
}

function confirmationHandler<T>(result: AxiosResponse<T>): ApiResponse<T> {
    const message = result.data as T;
    return {
        code: result.status,
        response: message
    } as ApiResponse<T>;
}

function errorHandler(error: AxiosError): ApiResponse<ErrorBody> {
    const result = error.response;
    if (error.response !== undefined) {
        return {
            code: parseInt(error.code),
            response: {
                error: result.data.message,
            },
        } as ApiResponse<ErrorBody>;
    } else {
        return {
            code: parseInt(error.code),
            response: {
                error: error.message
            }
        }
    }
}

function getAxiosConfig(): AxiosRequestConfig {
    const cookie = Cookies.get('token');
    return {
        headers: {
            'Authorization': `Bearer ${cookie}`
        }
    } as AxiosRequestConfig;
}

class ApiClientImpl implements ApiClient {
    private static api: ApiClient;

    private constructor() {
    }

    public static getInstance(): ApiClient {
        ApiClientImpl.api = new ApiClientImpl();
        return ApiClientImpl.api;
    }

    async currentStage(): Promise<ApiResponse<StageResponse>> {
        return await axios.get<StageResponse>(`http://${getURL()}/api/stage/current`, getAxiosConfig())
            .then(confirmationHandler)
            .catch(errorHandler);
    }

    async checkIfEnabled(what: string): Promise<ApiResponse<EnabledResponse>> {
        if (what === 'register') {
            return await axios.get<EnabledResponse>(`http://${getURL()}/api/${what}/isEnabled`)
                .then(confirmationHandler)
                .catch(errorHandler);
        } else {
            return await axios.get<EnabledResponse>(`http://${getURL()}/api/${what}/isEnabled`, getAxiosConfig())
                .then(confirmationHandler)
                .catch(errorHandler);
        }
    }

    async login(creds: LoginCredentials): Promise<ApiResponse<LoginResponse>> {
        return await axios.post<LoginResponse>(`http://${getURL()}/api/login`, creds, {})
            .then(confirmationHandler)
            .catch(errorHandler);
    }

    async registration(credentials: RegistrationCredentials): Promise<ApiResponse<ConfirmationResponse>> {
        return await axios.post<ConfirmationResponse>(`http://${getURL()}/api/register`, credentials)
            .then(confirmationHandler)
            .catch(errorHandler);
    }

    async changePassword(credentials: ChangePasswordCredentials): Promise<ApiResponse<ConfirmationResponse>> {
        return await axios.post<ConfirmationResponse>(`http://${getURL()}/api/profile/my/changePassword`, credentials, getAxiosConfig())
            .then(confirmationHandler)
            .catch(errorHandler);
    }

    async currentUser(): Promise<ApiResponse<UserResponse>> {
        return await axios.get<UserResponse>(`http://${getURL()}/api/profile/my`, getAxiosConfig())
            .then(confirmationHandler)
            .catch(errorHandler);
    }

    async updateUser(user: UserUpdate): Promise<ApiResponse<ConfirmationResponse>> {
        return await axios.post<ConfirmationResponse>(`http://${getURL()}/api/profile/my/edit`, user, getAxiosConfig())
            .then(confirmationHandler)
            .catch(errorHandler);
    }

    async sendInvitation(invitationID: number): Promise<ApiResponse<ConfirmationResponse>> {
        return await axios.post<ConfirmationResponse>
        (
            `http://${getURL()}/api/invitations/send`,
            {invitationId: invitationID},
            getAxiosConfig()
        ).then(confirmationHandler).catch(errorHandler);
    }

    async getInvitations(incoming: boolean): Promise<ApiResponse<InvitationsResponse>> {
        const boxType = incoming ? "inbox" : "outbox";
        return await axios.get<InvitationsResponse>(
            `http://${getURL()}/api/invitations/${boxType}`,
            getAxiosConfig()).then(confirmationHandler).catch(errorHandler);
    }

    async acceptInvitation(id: number): Promise<ApiResponse<ConfirmationResponse>> {
        return await axios.post<ConfirmationResponse>
        (
            `http://${getURL()}/api/invitations/accept/${id}`,
            {},
            getAxiosConfig()
        ).then(confirmationHandler).catch(errorHandler);
    }

    async rejectInvitation(id: number): Promise<ApiResponse<ConfirmationResponse>> {
        return await axios.post<ConfirmationResponse>
        (
            `http://${getURL()}/api/invitations/reject/${id}`,
            {},
            getAxiosConfig()
        ).then(confirmationHandler).catch(errorHandler);
    }

    async invitationInfo(): Promise<ApiResponse<InvitationInfoResponse>> {
        return await axios.get<InvitationInfoResponse>(`http://${getURL()}/api/invitations/info`, getAxiosConfig())
            .then(confirmationHandler).catch(errorHandler);
    }

    async getTaskById(id: number): Promise<ApiResponse<TaskResponse>> {
        return await axios.get<TaskResponse>(`http://${getURL()}/api/tasks/${id}`, getAxiosConfig())
            .then(confirmationHandler).catch(errorHandler);
    }

    async canAnswer(): Promise<ApiResponse<ConfirmationResponse>> {
        return await axios.get<ConfirmationResponse>(`http://${getURL()}/api/tasks/canAnswer`, getAxiosConfig())
            .then(confirmationHandler)
            .catch(errorHandler);
    }

    async answer(id: number, answer: string): Promise<ApiResponse<TaskAnswerConfirmation>> {
        return await axios.post<TaskAnswerConfirmation>(`http://${getURL()}/api/tasks/${id}/answer`, {answer: answer}, getAxiosConfig())
            .then(confirmationHandler)
            .catch(errorHandler);
    }

    async getNotifications(): Promise<ApiResponse<NotificationsResponse>> {
        return await axios.get<NotificationsResponse>(`http://${getURL()}/api/notifications/new`, getAxiosConfig())
            .then(confirmationHandler)
            .catch(errorHandler);
    }

    async readNotification(id: number): Promise<ApiResponse<ConfirmationResponse>> {
        return await axios.post<ConfirmationResponse>
        (
            `http://${getURL()}/api/notifications/seen/${id}`,
            {},
            getAxiosConfig()
        ).then(confirmationHandler).catch(errorHandler);
    }

    async getNews(): Promise<ApiResponse<NewsResponse>> {
        return await axios.get<NewsResponse>(`http://${getURL()}/api/news/all`, getAxiosConfig())
            .then(confirmationHandler)
            .catch(errorHandler);
    }

    async getEmployees(): Promise<ApiResponse<EmployeeResponse>> {
        return await axios.get<EmployeeResponse>(`http://${getURL()}/api/staff/all`, getAxiosConfig())
            .then(confirmationHandler)
            .catch(errorHandler);
    }

    async getTeam(): Promise<ApiResponse<TeamResponse>> {
        return await axios.get<TeamResponse>(`http://${getURL()}/api/teams/my`, getAxiosConfig())
            .then(confirmationHandler)
            .catch(errorHandler);
    }

    async changeTeamName(newName: string): Promise<ApiResponse<ConfirmationResponse>> {
        return await axios.post<ConfirmationResponse>
        (
            `http://${getURL()}/api/teams/my/edit`,
            {name: newName},
            getAxiosConfig()
        ).then(confirmationHandler).catch(errorHandler);
    }

    async getLeaderboard(): Promise<ApiResponse<LeaderboardResponse>> {
        return await axios.get<LeaderboardResponse>(
            `http://${getURL()}/api/leaderboard/get`,
            getAxiosConfig()
        ).then(confirmationHandler).catch(errorHandler);
    }

    async getItems(): Promise<ApiResponse<ItemResponse>> {
        return await axios.get<ItemResponse>(`http://${getURL()}/api/shop/all`, getAxiosConfig())
            .then(confirmationHandler)
            .catch(errorHandler);
    }

    async getPurchases(): Promise<ApiResponse<PurchaseResponse>> {
        return await axios.get<PurchaseResponse>(`http://${getURL()}/api/profile/my/purchases`, getAxiosConfig())
            .then(confirmationHandler)
            .catch(errorHandler);
    }

    async buyItem(id: number): Promise<ApiResponse<ConfirmationResponse>> {
        return await axios.post<ConfirmationResponse>
        (
            `http://${getURL()}/api/shop/purchase/${id}`,
            {},
            getAxiosConfig()
        ).then(confirmationHandler).catch(errorHandler);
    }
}

export default function api(): ApiClient {
    return ApiClientImpl.getInstance();
}
