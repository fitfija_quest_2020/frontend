import api from "./api";
import Cookies from "js-cookie";
import {
    LoginCredentials,
    ErrorBody,
    LoginResponseBody,
    RegistrationCredentials,
    Confirmation
} from "../utils/interfaces";

const ACCESS_TOKEN_COOKIE = "token";

export async function login(credentials: LoginCredentials): Promise<Confirmation> {
    return api().login(credentials)
        .then((resp) => {
            const {response, code} = resp;
            if (code === 200) {
                const {token} = response as LoginResponseBody;
                Cookies.set(ACCESS_TOKEN_COOKIE, token, {expires: 3600});
                return true;
            } else {
                const {error} = response as ErrorBody;
                return error;
            }
        });
}

export async function registration(credentials: RegistrationCredentials): Promise<Confirmation> {
    return api().registration(credentials).then(result => {
        const {response, code} = result;
        if (code === 201) {
            return true;
        } else {
            const {error} = response as ErrorBody;
            return error;
        }
    })
}

export function isLogged(): boolean {
    const jwt = Cookies.get(ACCESS_TOKEN_COOKIE);
    return typeof jwt != "undefined";
}

export function logout(): void {
    Cookies.remove(ACCESS_TOKEN_COOKIE);
}
