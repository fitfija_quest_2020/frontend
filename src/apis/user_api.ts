import {
    ApiResponse,
    ChangePasswordCredentials,
    Confirmation,
    ConfirmationResponse,
    ErrorBody,
    Invitation,
    InvitationInfo,
    InvitationInfoDto,
    Invitations,
    IsEnabled,
    Item,
    ItemDto,
    Leaderboard,
    LeaderboardEntry,
    NotificationDto,
    Notifications,
    Purchase, PurchaseDto,
    Team,
    TeamDto,
    UserResponse,
    UserUpdate
} from "../utils/interfaces";
import api from "./api";

export function confimationHandler(result: ApiResponse<ConfirmationResponse>, resCode: number) {
    const {response, code} = result;
    if (code === resCode) {
        return true;
    }
    const {error} = response as ErrorBody;
    return error;
}

export async function currentUser(): Promise<UserResponse> {
    return await api().currentUser().then(result => {
        return result.response;
    })
}

export async function currentTeam(): Promise<Team> {
    return await api().getTeam().then(result => {
        const {response, code} = result;
        if (code === 200) {
            return response as TeamDto;
        } else {
            const {error} = response as ErrorBody;
            return error;
        }
    })
}

export async function sendInvitation(invitationID: number): Promise<Confirmation> {
    return api().sendInvitation(invitationID).then(result => confimationHandler(result, 201));
}

export async function updateUser(credentials: UserUpdate): Promise<Confirmation> {
    return api().updateUser(credentials).then(result => confimationHandler(result, 200));
}

export async function updateTeam(newName: string): Promise<Confirmation> {
    return api().changeTeamName(newName).then(result => confimationHandler(result, 200));
}

export async function isEnabled(what: string): Promise<boolean> {
    return api().checkIfEnabled(what).then(result => {
        const {response, code} = result;
        if (code === 200) {
            return (response as IsEnabled).result as boolean;
        } else {
            return false;
        }
    });
}

export async function getInvitations(incoming: boolean): Promise<Invitations> {
    return api().getInvitations(incoming).then(result => {
        const {code, response} = result;
        if (code === 200) {
            return response as Invitation[];
        }
        return (response as ErrorBody).error;
    })
}

export async function acceptInvitation(id: number): Promise<Confirmation> {
    return api().acceptInvitation(id).then(result => confimationHandler(result, 200));
}

export async function rejectInvitation(id: number): Promise<Confirmation> {
    return api().rejectInvitation(id).then(result => confimationHandler(result, 200));
}

export async function invitationInfo(): Promise<InvitationInfo> {
    return api().invitationInfo().then(result => {
        if (result.code === 200) {
            return result.response as InvitationInfoDto;
        }
        return (result.response as ErrorBody).error as string;
    })
}

export async function getNotifications(): Promise<Notifications> {
    return api().getNotifications().then(result => {
        if (result.code === 200) {
            return result.response as NotificationDto[];
        }
        return (result.response as ErrorBody).error as string;
    });
}

export async function readNotification(id: number): Promise<Confirmation> {
    return api().readNotification(id).then(result => confimationHandler(result, 200));
}

export async function changePassword(cred: ChangePasswordCredentials): Promise<Confirmation> {
    return api().changePassword(cred).then(result => confimationHandler(result, 200));
}

export async function buyItem(id: number): Promise<Confirmation> {
    return api().buyItem(id).then(result => confimationHandler(result, 200));
}

export async function getLeaderboard(): Promise<Leaderboard> {
    return api().getLeaderboard().then(lb => {
        if (lb.code === 200) {
            return lb.response as LeaderboardEntry[];
        }
        return (lb.response as ErrorBody).error as string;
    })
}

export async function getShop(): Promise<Item> {
    return api().getItems().then(item => {
        if (item.code === 200) {
            return item.response as ItemDto[];
        }
        return (item.response as ErrorBody).error as string;
    })
}

export async function getPurchases(): Promise<Purchase> {
    return api().getPurchases().then(purchase => {
        if (purchase.code === 200) {
            return purchase.response as PurchaseDto[];
        }
        return (purchase.response as ErrorBody).error as string;
    })
}
