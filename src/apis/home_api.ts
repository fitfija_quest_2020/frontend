import {EmployeeDto, Employees, ErrorBody, News, NewsDto} from "../utils/interfaces";
import api from "./api";

export async function getNews(): Promise<News> {
    return api().getNews().then(result => {
        if (result.code === 200) {
            return result.response as NewsDto[];
        }
        return (result.response as ErrorBody).error as string;
    });
}


export async function getEmployees(): Promise<Employees> {
    return api().getEmployees().then(result => {
        if (result.code === 200) {
            return result.response as EmployeeDto[];
        }
        return (result.response as ErrorBody).error as string;
    });
}
